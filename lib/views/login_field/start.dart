import 'dart:ui';
import 'package:flutter/material.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({Key? key}) : super(key: key);

  @override
  State<StartScreen> createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            Container(
              child: Image(
                image: AssetImage("images/logo.png"),
                width: 250,
                height: 250,
              ),
            ),
            Container(
              child: Image(image: AssetImage("images/logo2.png")),
            ),
          ],
        ),
      ),
    );
  }
}
